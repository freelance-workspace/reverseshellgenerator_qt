#ifndef QTDOWNLOAD_H
#define QTDOWNLOAD_H

#include <QObject>
#include <QString>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QFile>


class QtDownload : public QObject {
    Q_OBJECT
public:
    explicit QtDownload(QObject *parent = nullptr);
    ~QtDownload();

    void setTarget(const QString& t);

    const QByteArray &getDataArray() const;

private:
    QNetworkAccessManager manager;
    QString target;
    QFile localFile;
    QByteArray dataArray;

signals:
    void done();
    void done(QByteArray array);

public slots:
    void download();
    void downloadFinished(QNetworkReply* data);
    void downloadProgress(qint64 recieved, qint64 total);
};

#endif // QTDOWNLOAD_H
