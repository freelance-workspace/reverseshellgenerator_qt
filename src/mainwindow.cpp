﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVector>
#include <QMultiMap>
#include <QDebug>
#include <QClipboard>
#include <QFileDialog>
#include <QDialogButtonBox>
#include <QFontDialog>
#include "qtdownload.h"



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle("Reverse Shell Generator");
    QObject::connect(&m_Download, SIGNAL(done(QByteArray)), this, SLOT(Download(QByteArray)));

    mURLaddres = "http://127.0.0.1/wera/update.json";

        QFile file;
        file.setFileName(":/update.json");
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        readJson(file.readAll());
        file.close();


        QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
        // You may want to use QRegularExpression for new code with Qt 5 (not mandatory).
        QRegExp ipRegex ("^" + ipRange
        + "\\." + ipRange
        + "\\." + ipRange
        + "\\." + ipRange + "$");
        QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);
        ui->lineEdit_IP->setValidator(ipValidator);


        QMapIterator<QString, QMap<QString, QString>> it(CommandType);
        while (it.hasNext())
        {
            it.next();
            ui->comboBox_CommandType->addItem(it.key());
            //qDebug() << "key: " <<  it.key() << "  value: " << it.value();
        }

        QMapIterator<QString,QString> it2(listenerCommands);
        while (it2.hasNext())
        {
            it2.next();
            ui->comboBox_listenerCommands->addItem(it2.key());
            //qDebug() << "key: " <<  it.key() << "  value: " << it.value();
        }
        /**/

        QMapIterator<QString,QString> it3(CommandType_OS["ReverseShell"]);
        while (it3.hasNext())
        {
            it3.next();
            //ui->comboBox_listenerCommands->addItem(it2.key());
            qDebug() << "key: " <<  it3.key() << "  value: " << it3.value();
        }
}

MainWindow::~MainWindow()
{
    delete ui;
}



    void MainWindow::readJson(QString val)
    {

        qDebug() << "------------------------------------------";
        QJsonDocument document = QJsonDocument::fromJson(val.toUtf8());


        // Taking from the document root object
        QJsonObject root = document.object();


        //-----------------------------------------------------------//

        CommandType["ReverseShell"].clear();
        CommandType_OS["ReverseShell"].clear();
        // The second value prescribe line
        QJsonValue jv = root.value("ReverseShell");
        // If the value is an array, ...
        if(jv.isArray()){
            // ... then pick from an array of properties
            QJsonArray ja = jv.toArray();
            // Going through all the elements of the array ...
            for(int i = 0; i < ja.count(); i++)
            {
                QJsonObject subtree = ja.at(i).toObject();
                // Taking the values of the properties and last name by adding them to textEdit
                auto name = subtree.value("name").toString();
                auto command = subtree.value("command").toString();
                auto array_OS = subtree.value("meta");


                CommandType["ReverseShell"].insert(name,command);
                qDebug() << name << "  " << command << "  ";


                if(array_OS.isArray())
                {
                    QJsonArray ja_OS = array_OS.toArray();
                    for(int i = 0; i < ja_OS.count(); i++)
                    {
                        CommandType_OS["ReverseShell"].insertMulti(name,ja_OS.at(i).toString());
                        //QJsonObject sub_tree = ja_OS.at(i).toObject();
                        qDebug() << ja_OS.at(i).toString();
                    }
                }
            }
        }

        //--------------------------------------------------------------//

        CommandType["BindShell"].clear();
        CommandType_OS["BindShell"].clear();
        // The second value prescribe line
        jv = root.value("BindShell");
        // If the value is an array, ...
        if(jv.isArray())
        {
            // ... then pick from an array of properties
            QJsonArray ja = jv.toArray();
            // Going through all the elements of the array ...
            for(int i = 0; i < ja.count(); i++)
            {
                QJsonObject subtree = ja.at(i).toObject();
                // Taking the values of the properties and last name by adding them to textEdit
                auto name = subtree.value("name").toString();
                auto command = subtree.value("command").toString();
                auto array_OS = subtree.value("meta");

                CommandType["BindShell"].insert(name,command);

                qDebug() << "BindShell:  " << name << "  " << command << "  ";


                if(array_OS.isArray())
                {
                    QJsonArray ja_OS = array_OS.toArray();
                    for(int i = 0; i < ja_OS.count(); i++)
                    {
                        CommandType_OS["BindShell"].insertMulti(name,ja_OS.at(i).toString());
                        //QJsonObject sub_tree = ja_OS.at(i).toObject();
                        qDebug() << ja_OS.at(i).toString();
                    }
                }
            }
        }

        //--------------------------------------------------------------//

        CommandType["MSFVenom"].clear();
        CommandType_OS["MSFVenom"].clear();
        // The second value prescribe line
        jv = root.value("MSFVenom");
        // If the value is an array, ...
        if(jv.isArray())
        {
            // ... then pick from an array of properties
            QJsonArray ja = jv.toArray();
            // Going through all the elements of the array ...
            for(int i = 0; i < ja.count(); i++)
            {
                QJsonObject subtree = ja.at(i).toObject();
                // Taking the values of the properties and last name by adding them to textEdit
                auto name = subtree.value("name").toString();
                auto command = subtree.value("command").toString();
                auto array_OS = subtree.value("meta");
                CommandType["MSFVenom"].insert(name,command);

                qDebug() << "MSFVenom:  " << name << "  " << command << "  ";


                if(array_OS.isArray())
                {
                    QJsonArray ja_OS = array_OS.toArray();
                    for(int i = 0; i < ja_OS.count(); i++)
                    {
                        CommandType_OS["MSFVenom"].insertMulti(name,ja_OS.at(i).toString());
                        //QJsonObject sub_tree = ja_OS.at(i).toObject();
                        qDebug() << ja_OS.at(i).toString();
                    }
                }
            }
        }

        //--------------------------------------------------------------//

        listenerCommands.clear();
        // The second value prescribe line
        jv = root.value("listenerCommands");
        // If the value is an array, ...
        if(jv.isArray())
        {
            // ... then pick from an array of properties
            QJsonArray ja = jv.toArray();
            // Going through all the elements of the array ...
            for(int i = 0; i < ja.count(); i++)
            {
                QJsonObject subtree = ja.at(i).toObject();
                // Taking the values of the properties and last name by adding them to textEdit
                auto name = subtree.value("name").toString();
                auto command = subtree.value("command").toString();
                listenerCommands.insert(name,command);

                qDebug() << "listenerCommands:  " << name << "  " << command << "  ";
            }
        }

    }


    bool MainWindow::IsCheck_OS(QString key)
    {
        QString all_os;
        for(const auto &it : CommandType_OS[CommandType_Key].values(key))
        {
            all_os += it +",";
        }

        qDebug() << all_os << " " << ui->checkBox_Windows->isChecked();

        if(ui->checkBox_Windows->isChecked() && all_os.contains("windows")) return true;
        if(ui->checkBox_Linux->isChecked() && all_os.contains("linux")) return true;
        if(ui->checkBox_Mac->isChecked() && all_os.contains("mac")) return true;

        return false;
    }

    void MainWindow::UpdateItemList()
    {
        ui->listWidget->clear();
        QMapIterator<QString, QString> it(CommandType[CommandType_Key]);
        while (it.hasNext())
        {
            it.next();
            if(IsCheck_OS(it.key()))
            {
                ui->listWidget->addItem(it.key());
                qDebug() << CommandType_OS.values(it.key());
                qDebug() << "key: " <<  it.key() << "  value: " << it.value();
            }
        }
    }

    void MainWindow::UpdateTextToReplace()
    {
        QString IP = ui->lineEdit_IP->text();
        QString Port = QString::number(ui->spinBox_PORT->value());
        QString Shell = ui->comboBox_Shell->currentText();

        QString result_recovery = str_info_command;

        result_recovery.replace("{ip}" , IP);
        result_recovery.replace("{port}" , Port);
        result_recovery.replace("{shell}" , Shell);

        qDebug() << result_recovery;

        ui->plainTextEdit->document()->setPlainText(result_recovery);
    }

    void MainWindow::UpdateListener()
    {
        QString result_listener_recovery = str_info_listener;

        QString IP = ui->lineEdit_IP->text();
        QString Port = QString::number(ui->spinBox_PORT->value());
        QString Shell = ui->comboBox_Shell->currentText();
        result_listener_recovery.replace("{ip}" , IP);
        result_listener_recovery.replace("{port}" , Port);
        result_listener_recovery.replace("{shell}" , Shell);

        ui->label->setText(result_listener_recovery);
    }


    void MainWindow::on_listWidget_currentTextChanged(const QString &currentText)
    {
        str_info_command = CommandType[CommandType_Key][currentText];
        UpdateTextToReplace();
    }


    void MainWindow::on_comboBox_Shell_currentTextChanged(const QString &arg1)
    {
        UpdateTextToReplace();
    }


    void MainWindow::on_comboBox_CommandType_currentTextChanged(const QString &arg1)
    {
       CommandType_Key = arg1;
       UpdateItemList();
    }


    void MainWindow::on_comboBox_listenerCommands_currentTextChanged(const QString &arg1)
    {
        str_info_listener = listenerCommands[arg1];
        UpdateListener();
    }





void MainWindow::on_lineEdit_IP_textEdited(const QString &arg1)
{
   UpdateTextToReplace();
   UpdateListener();
}


void MainWindow::on_spinBox_PORT_valueChanged(int arg1)
{
   UpdateTextToReplace();
   UpdateListener();
}


void MainWindow::on_pushButton_2_clicked()
{
    ui->plainTextEdit->clear();
}


void MainWindow::on_pushButton_3_clicked()
{
    QApplication::clipboard()->setText(ui->label->text());
}


void MainWindow::on_pushButton_clicked()
{
    QApplication::clipboard()->setText(ui->plainTextEdit->document()->toPlainText());
}


void MainWindow::on_checkBox_Windows_stateChanged(int arg1)
{
    UpdateItemList();
}


void MainWindow::on_checkBox_Linux_stateChanged(int arg1)
{
    UpdateItemList();
}


void MainWindow::on_checkBox_Mac_stateChanged(int arg1)
{
    UpdateItemList();
}


void MainWindow::on_actionload_json_triggered()
{
    auto file1Name = QFileDialog::getOpenFileName(this, tr("Open JSON File 1"), "/home", tr("JSON Files (*.json)"));

    QFile file;
    file.setFileName(file1Name);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    readJson(file.readAll());
    file.close();

    UpdateItemList();
    ui->comboBox_CommandType->setCurrentText("ReverseShell");
}


void MainWindow::on_actionUpdate_to_Site_triggered()
{
   m_Download.setTarget(mURLaddres);
   m_Download.download();
}


void MainWindow::on_pushButton_4_clicked()
{
   m_Download.setTarget(mURLaddres);
   m_Download.download();
}

void MainWindow::Download(QByteArray array)
{
    qDebug() << array;
    readJson(array);
    UpdateItemList();
}


void MainWindow::on_actionSettings_triggered()
{
    QDialog dlg(this);
    dlg.setWindowTitle(tr("My dialog"));

    QLineEdit *ledit1 = new QLineEdit(&dlg);
    QDialogButtonBox *btn_box = new QDialogButtonBox(&dlg);
    btn_box->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    connect(btn_box, &QDialogButtonBox::accepted, &dlg, &QDialog::accept);
    connect(btn_box, &QDialogButtonBox::rejected, &dlg, &QDialog::reject);

    ledit1->setPlaceholderText(mURLaddres);


    QFormLayout *layout = new QFormLayout();
    layout->addRow(tr("URL data *json :"), ledit1);
    layout->addWidget(btn_box);

    dlg.setLayout(layout);

    // В случае, если пользователь нажал "Ok".
    if(dlg.exec() == QDialog::Accepted)
    {
        const QString &str1 = ledit1->text();
        if(str1.size() > 2)
        {
          mURLaddres = str1;
          qDebug() << str1 ;
        }
    }
}


void MainWindow::on_pushButton_Font_clicked()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, QFont("Helvetica [Cronyx]", 10), this);
    if (ok)
    {
        // the user clicked OK and font is set to the font the user selected
        ui->plainTextEdit->setFont(font);
    }
    else
    {
        // the user canceled the dialog; font is set to the initial
        // value, in this case Helvetica [Cronyx], 10
    }
}

