#include "qtdownload.h"

#include "qtdownload.h"

#include <QUrl>
#include <QNetworkRequest>
#include <QFile>

#include <QDebug>

QtDownload::QtDownload(QObject *parent)
    : QObject(parent) ,
      localFile("downloadedfile")
{
    QObject::connect(&manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(downloadFinished(QNetworkReply*)));
}

QtDownload::~QtDownload() {

}


void QtDownload::setTarget(const QString &t) {
    this->target = t;
}


const QByteArray &QtDownload::getDataArray() const
{
    return dataArray;
}

void QtDownload::downloadFinished(QNetworkReply *data)
{
    if (!localFile.open(QIODevice::WriteOnly))
        return;
    const QByteArray dataArrayy = data->readAll();
    dataArray = dataArrayy;
    localFile.write(dataArray);
    //qDebug() << dataArray;
    localFile.close();

    emit done(dataArray);
    emit done();
    delete data;
    data = nullptr;
}

void QtDownload::download()
{
    QUrl url = QUrl::fromEncoded(this->target.toLocal8Bit());
    QNetworkRequest request(url);
    QObject::connect(manager.get(request), SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));

}

void QtDownload::downloadProgress(qint64 recieved, qint64 total)
{
    qDebug() << recieved << total;
}
