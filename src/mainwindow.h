#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QtGlobal>
#include <QDebug>
#include "qtdownload.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool IsCheck_OS(QString key);
    void readJson(QString val);
    void UpdateItemList();
    void UpdateTextToReplace();
    void UpdateListener();

private slots:
    void on_listWidget_currentTextChanged(const QString &currentText);

    void on_comboBox_Shell_currentTextChanged(const QString &arg1);

    void on_comboBox_CommandType_currentTextChanged(const QString &arg1);

    void on_comboBox_listenerCommands_currentTextChanged(const QString &arg1);

    void on_lineEdit_IP_textEdited(const QString &arg1);

    void on_spinBox_PORT_valueChanged(int arg1);

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_checkBox_Windows_stateChanged(int arg1);

    void on_checkBox_Linux_stateChanged(int arg1);

    void on_checkBox_Mac_stateChanged(int arg1);

    void on_actionload_json_triggered();

    void on_actionUpdate_to_Site_triggered();

    void on_pushButton_4_clicked();

    void Download(QByteArray array);

    void on_actionSettings_triggered();

    void on_pushButton_Font_clicked();

private:
    Ui::MainWindow *ui;
    QString str_info_command;
    QString str_info_listener;
    QString CommandType_Key;
    QString mURLaddres;

    QtDownload m_Download;
    QMap<QString, QString> listenerCommands;
    QMap<QString, QMap<QString, QString>> CommandType_OS;
    QMap<QString, QMap<QString, QString>> CommandType;


};
#endif // MAINWINDOW_H
